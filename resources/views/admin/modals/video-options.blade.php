<div class="custom-control autoplay custom-checkbox">
  <input name="autoplay" value="1" type="checkbox" class="custom-control-input check cursor" id="autoplay">
  <label class="custom-control-label cursor" for="autoplay">Autoplay</label>
</div>
<div class="custom-control loop custom-checkbox">
  <input name="loop" value="1" type="checkbox" class="custom-control-input check cursor" id="loop">
  <label class="custom-control-label cursor" for="loop">Loop</label>
</div>
<div class="custom-control controls custom-checkbox">
  <input name="controls" value="1" type="checkbox" class="custom-control-input check cursor" id="controls">
  <label class="custom-control-label cursor" for="controls">Controls</label>
</div>
<div class="custom-control mute custom-checkbox">
  <input name="mute" value="1" type="checkbox" class="custom-control-input check cursor" id="mute">
  <label class="custom-control-label cursor" for="mute">Mute</label>
</div>
<div class="custom-control related custom-checkbox remove-for-vimeo remove-for-upload">
  <input name="related_videos" value="1" type="checkbox" class="custom-control-input check cursor" id="related_videos">
  <label class="custom-control-label cursor" for="related_videos">Hide Related Videos</label>
</div>
<div class="custom-control fullscreen custom-checkbox remove-for-upload">
  <input name="fullscreen" value="1" type="checkbox" class="custom-control-input check cursor" id="fullscreen">
  <label class="custom-control-label cursor" for="fullscreen">Allow Fullscreen</label>
</div>
<div class="custom-control frameborder custom-checkbox remove-for-upload">
  <input name="frameborder" value="1" type="checkbox" class="custom-control-input check cursor" id="frameborder">
  <label class="custom-control-label cursor" for="frameborder">Frameborder</label>
</div>
<div class="custom-control gyroscope  custom-checkbox remove-for-vimeo remove-for-upload">
  <input name="gyroscope" value="1" type="checkbox" class="custom-control-input check cursor" id="gyroscope">
  <label class="custom-control-label cursor" for="gyroscope">Gyroscope</label>
</div>
<div class="custom-control accelerometer custom-checkbox remove-for-vimeo remove-for-upload">
  <input name="accelerometer" value="1" type="checkbox" class="custom-control-input check cursor" id="accelerometer">
  <label class="custom-control-label cursor" for="accelerometer">Accelerometer</label>
</div>
<div class="custom-control picture custom-checkbox remove-for-vimeo remove-for-upload">
  <input name="picture" value="1" type="checkbox" class="custom-control-input check cursor" id="picture">
  <label class="custom-control-label cursor" for="picture">Picture-in-picture</label>
</div>
<div class="custom-control encrypt custom-checkbox remove-for-vimeo remove-for-upload">
  <input name="encrypt_media" value="1" type="checkbox" class="custom-control-input check cursor" id="encrypt_media">
  <label class="custom-control-label cursor" for="encrypt_media">Encrypted Media</label>
</div>
<div class="custom-control portrait custom-checkbox remove-for-youtube remove-for-upload">
  <input name="portrait" value="1" type="checkbox" class="custom-control-input check cursor" id="portrait">
  <label class="custom-control-label cursor" for="portrait">Portrait</label>
</div>
<div class="custom-control title custom-checkbox remove-for-youtube remove-for-upload">
  <input name="title" value="1" type="checkbox" class="custom-control-input check cursor" id="title">
  <label class="custom-control-label cursor" for="title">Title</label>
</div>
<div class="custom-control byline custom-checkbox remove-for-youtube remove-for-upload">
  <input name="byline" value="1" type="checkbox" class="custom-control-input check cursor" id="byline">
  <label class="custom-control-label cursor" for="byline">Byline</label>
</div>
<div class="custom-control start custom-checkbox remove-for-upload">
  <input value="1" type="checkbox" class="custom-control-input check cursor" id="start">
  <label class="custom-control-label cursor" for="start">Start Video at</label>
  <input name="start" type="text" id="timeInput" class="timecode txt_sm inline_input" value="00:00" data-seconds="2">
</div>